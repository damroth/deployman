USE flask;
CREATE TABLE todo_v3 LIKE todo_v2; 
INSERT INTO  todo_v3 SELECT * FROM todo_v2;
CREATE TRIGGER migration_insert AFTER INSERT ON todo_v2 for each row INSERT INTO todo_v3 (id, title ,complete) Values (new.id, new.title, new.complete);
CREATE TRIGGER migration_update AFTER UPDATE ON todo_v2 for each row UPDATE todo_v3 SET title = new.title, complete = new.complete WHERE id = new.id;
CREATE TRIGGER migration_delete AFTER DELETE ON todo_v2 for each row DELETE FROM todo_v3 WHERE id = old.id;