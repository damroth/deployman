from modules.connectors.ssh_connector import SshConn
from configparser import ConfigParser
from modules.infra.lb import Lb
import pickle

class Deployer:

    def __init__(self, config: ConfigParser, instance, ssh: SshConn):

        self.config = config
        self.app_container = self.config['app']['container_name']
        self.app_version = self.config['app']['version']
        self.app_name = instance
        self.app_id = ""
        self.app_port = ""
        self.ssh = ssh
        self.env = f"-e MYSQL_HOST={self.config['proxysql']['ssh_host']} \
            -e MYSQL_USER={self.config['app']['mysql_user']} \
            -e MYSQL_PASSWORD={self.config['app']['mysql_passwd']}"


    def deploy(self, callback_lb: Lb, deploy_config=True):
        print(f"[i] Deploying app {self.app_name}")
        if not self.ssh.run_command(f"docker port {self.app_name}").split(':::')[-1].rstrip():
            cmd = f'docker run --rm -d --name {self.app_name} -p 5000 {self.env} {self.app_container}:{self.app_version}'
            self.app_id = self.ssh.run_command(cmd)
            self.app_port = self.ssh.run_command(f"docker port {self.app_id}").split(':::')[-1].rstrip()
            callback_lb.add_app(self.ssh.host, self.app_port)
            if deploy_config:
                callback_lb.deploy_config()
                callback_lb.reload_service()
            print(f"[+] App {self.app_name} has been deployed")
            state = DeployerState()
            state.apps[self.app_name] = {
                'id': self.app_id, 'host': self.ssh.host, 'port': self.app_port, 'ssh_user': self.ssh.username, 'version': self.app_version
                }
            state.save()
        else:
            print(f"[i] App {self.app_name} already running. Skipping")


    def destroy(self, callback_lb: Lb, deploy_config=True) -> bool:
        print(f"[i] Stopping app {self.app_name}")
        if self.verify():
            callback_lb.remove_app(self.ssh.host, self.app_port)
            if deploy_config:           
                callback_lb.deploy_config()
                callback_lb.reload_service()     
            self.ssh.run_command(f'docker stop {self.app_name}')
            print("[+] App has been removed")
            state = DeployerState()
            del state.apps[self.app_name]
            state.save()
        else:
            print(f"[i] App {self.app_name} already stopped")


    def verify(self) -> bool:
        self.app_id = self.ssh.run_command(f'docker ps --no-trunc -aqf "name=^{self.app_name}$"')
        if self.app_id == '':
            return False
        self.app_port = self.ssh.run_command(f"docker port {self.app_id}").split(':::')[-1].rstrip()
        state = DeployerState()
        if self.app_id == state.apps[self.app_name]['id']:
            return True
        else:
            return False


class DeployerState:

    def __init__(self):

        self.apps = {}
        self.load()

    def load(self):
        with open('state/state.dpm', 'rb') as dpm:
            try:
                tmp = pickle.load(dpm)
                self.__dict__.update(tmp.__dict__)
            except:
                self._reset()


    def save(self):
        with open('state/state.dpm', 'wb') as dpm:
            pickle.dump(self, dpm, pickle.HIGHEST_PROTOCOL)


    def _reset(self):
        self.apps = {}
        self.save()
