from scp import SCPClient
import paramiko
import socket


class SshConn:
    """
        SSh Connector class.
        host: target hostname
        username: ssh username
        
        This class is not using a password. It requires that there is ssh key configured on host.
    """

    def __init__(self, host, username):
        self.host = host
        self.username = username
        self.client = paramiko.SSHClient()
        self.client.load_system_host_keys()
        self.client.set_missing_host_key_policy(paramiko.AutoAddPolicy)


    def test_connection(self) -> bool:
        """
            Simple connection test
            returns:  true or false
        """
        res = self._execute('echo "test"')
        if "test\n" in res:
            return True
        else:
            print("SSH communication failed")
            print(f"RES: {res}")
            return False


    def run_command(self, cmd: str) -> str:
        """
            Execute ssh command
            cmd: any shell command
            returns: result of a command
        """
        if not cmd:
            print("SSH command cannot be empty")
            exit(1)
        else:
            return ' '.join(self._execute(cmd))


    def scp_file(self, filename) -> None:
        """
            Copy file through ssh. It uses ssh class settings
            filename: a filename to copy
        """
        try:
            self.client.connect(hostname=self.host, username=self.username)
            scp = SCPClient(self.client.get_transport())
            scp.put(filename)
        except paramiko.ssh_exception.AuthenticationException:
            print(f"Invalid credentials for host: {self.host}")
        except socket.gaierror:
            print(f"Host {self.host} unreachable")


    def _execute(self, cmd: str) -> str:
        """
            Internal wrapper
            Execute ssh command
            cmd: any shell command
            returns: result of a command
        """
        try:
            self.client.connect(hostname=self.host, username=self.username)
            _, stdout, _ = self.client.exec_command(cmd)
        except paramiko.ssh_exception.AuthenticationException:
            print(f"Invalid credentials for host: {self.host}")
        except socket.gaierror:
            print(f"Host {self.host} unreachable")
        else:
            res = stdout.readlines()
            self.client.close()
            return res
