import mysql.connector
import time

class MySQLConn:
    """
        MySQL connector class wrapper.
        host: IPv4 address to connect to
        user: mysql username
        pass: mysql password
        port: mysql port number
        kind: target type: master, slave or proxy [guarded]
    """
    
    def __init__(self, host, user, password, port, kind):
        self.host       = host
        self.user       = user
        self.password   = password
        self.port       = port
        self.kind       = kind
        self.connection = ""
        self.cursor     = ""

        if self.kind != "master" and self.kind != "slave" and self.kind != "proxy": raise AttributeError('Invalid kind attribute')


    def connect(self) -> None:
        """
            Connect to target host
            sets self.cursor and self.connection
        """

        for _ in range(10):
            try:
                con = mysql.connector.connect(host=self.host, password=self.password, \
                    port=self.port, user=self.user)
                cursor = con.cursor()
            except Exception as e:
                print(f"[i] MySQL connector - Can't connect to MySQL server: {e}. Waiting for MySQL to up...")
                time.sleep(10)
            else:
                break
        else:
            print(f"[-] MySQL connector - Can't connect to MySQL Server: {self.host}")
            raise ConnectionError(f"[-] MySQL connector - Can't connect to MySQL Server: {self.host}")
            
        print(f"[+] MySQL connector - Connected to {self.host}")
        self.connection = con
        self.cursor = cursor


    def close(self) -> None:
        """
            Close active connection. It destroys self.cursor and self.connection
        """
        self.cursor.close()
        self.connection.close()
