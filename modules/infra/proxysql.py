from modules.connectors.ssh_connector import SshConn
from configparser import ConfigParser

class ProxySQL:

    def __init__(self, config: ConfigParser):
        self.config       = config
        self.ssh          = SshConn(self.config['proxysql']['ssh_host'], self.config['proxysql']['ssh_user'])
        self.pxsql_name   = 'proxysql'
        self.img_proxysql = 'percona/proxysql2:2.2'
        self.pxsql_port = '6032'

    def init_proxysql_container(self) -> None:
        print("[i] Deploying ProxySQL")
        
        proxysql = self.ssh.run_command('docker ps --filter "name="' + self.pxsql_name + ' --format "{{.Names}}"')
        if self.pxsql_name in proxysql:
            print("[i] ProxySQL container already running")
        else:
            proxysql = self.ssh.run_command('docker run --rm -d --name ' + self.pxsql_name + ' -p 3306:3306 '+ \
                self.img_proxysql)
            print(f"[+] ProxySQL has been deployed: {proxysql}")


    def configure_proxy(self) -> None:
        print("[i] Configuring ProxySQL")
        check = self.ssh.run_command('docker exec proxysql bash -c "mysql -uadmin -padmin -h 127.0.0.1 -P 6032 -e \'select * from runtime_mysql_servers\'"')
        if "ONLINE" in check:
            print("[i] ProxySQL already configured. Skipping")
            return
        elif "OFFLINE" in check:
            print("[-] ProxySQL already configured but is unable to connect to MySQL Servers. Skipping")
            return
        else:
            self._generate_sql_batch_file()
            self.ssh.scp_file('migrations/proxysql.sql')
            self.ssh.run_command('docker cp proxysql.sql proxysql://tmp/batch.sql')
            self.ssh.run_command('docker exec proxysql bash -c "mysql -uadmin -padmin -h 127.0.0.1 -P 6032 < /tmp/batch.sql"')
            print("[+] ProxySQL has been configured")


    
    def _generate_sql_batch_file(self):
    
        cmds = [f"INSERT INTO mysql_servers (hostgroup_id, hostname, port) VALUES (10, '{self.config['master']['host']}', \
            {self.config['master']['port']}); "]
        cmds.append(f"INSERT INTO mysql_servers (hostgroup_id, hostname, port) VALUES (20, '{self.config['slave']['host']}', \
            {self.config['slave']['port']}); ")
        cmds.append("INSERT INTO mysql_replication_hostgroups (writer_hostgroup, reader_hostgroup, comment) VALUES (10, 20, 'Master / Slave'); ")
        cmds.append("LOAD MYSQL SERVERS TO RUNTIME; SAVE MYSQL SERVERS TO DISK;")
        cmds.append(f"SET mysql-monitor_username = '{self.config['proxysql']['monitor_user']}';")
        cmds.append(f"SET mysql-monitor_password = '{self.config['proxysql']['monitor_passwd']}';")
        cmds.append("LOAD MYSQL VARIABLES TO RUNTIME; SAVE MYSQL VARIABLES TO DISK;")
        cmds.append(f"INSERT INTO mysql_users (username, password, default_hostgroup) VALUES ('{self.config['app']['mysql_user']}', '{self.config['app']['mysql_passwd']}', 10); ")
        cmds.append("LOAD MYSQL USERS TO RUNTIME; SAVE MYSQL USERS TO DISK;")

        f = open('migrations/proxysql.sql', 'w')
        f.writelines(cmds)
