from modules.connectors.mysql_connector import MySQLConn
from modules.connectors.ssh_connector import SshConn
from modules import helpers
from configparser import ConfigParser

class MySQL:

    def __init__(self, config: ConfigParser):
        
        self.config       = config
        self.img_mysql    = 'mariadb:10.6'
        self.d_mysql      = {'master': {'name': 'sql-master', 'port': self.config['master']['port'], \
                                'passwd': self.config['master']['mysql_root_passwd'], 'host': self.config['master']['host']},
                            'slave': {'name': 'sql-slave', 'port': self.config['slave']['port'], \
                                'passwd': self.config['slave']['mysql_root_passwd'], 'host': self.config['master']['host']}
                            }

        self.master_con = MySQLConn(
            self.d_mysql['master']['host'],
            'root',
            self.d_mysql['master']['passwd'],
            self.d_mysql['master']['port'],
            'master')
        
        self.slave_con = MySQLConn(
            self.d_mysql['slave']['host'],
            'root',
            self.d_mysql['slave']['passwd'],
            self.d_mysql['slave']['port'],
            'slave')


    def init_mysql_containers(self) -> None:

        print("[i] Creating MySQL containers")

        for server in self.d_mysql:
            ssh = SshConn(self.config[server]['host'], self.config[server]['ssh_user'])
            mysql = ssh.run_command('docker ps --filter "name="' + self.d_mysql[server]['name'] + ' --format "{{.Names}}"')
            if self.d_mysql[server]['name'] in mysql:
                print(f"[i] MySQL {self.d_mysql[server]['name']} container already running")
            else:
                mysql = ssh.run_command("docker run -p " + self.d_mysql[server]['port'] + ":3306 --name " + self.d_mysql[server]['name'] + \
                    " --rm -e MARIADB_ROOT_PASSWORD=" + self.d_mysql[server]['passwd'] + " -d " + self.img_mysql + \
                    " --log-bin=mysql-bin.log")
                print(f"[+] MySQL {self.d_mysql[server]['name']} deployed: {mysql}")    


    def setup_mysql_replication(self) -> bool:
        master_log_pos = ""
        master_binlog = ""
        password = helpers.gen_passwd()

        print("[i] Configuring MySQL replication")

        for server in self.d_mysql:
            if self.d_mysql[server]['name'] == 'sql-master':
                self.master_con.connect()
                self.master_con.cursor.execute("SELECT user FROM mysql.user WHERE user = 'replica_user'")
                res =  self.master_con.cursor.fetchall()
                if res == []:
                    self.master_con.cursor.execute('SET GLOBAL server_id=1')
                    self.master_con.cursor.execute(f"CREATE USER 'replica_user'@'%' IDENTIFIED BY '{password}'")
                    self.master_con.cursor.execute("GRANT REPLICATION SLAVE ON *.* TO 'replica_user'@'%'")
                    self.master_con.cursor.execute('FLUSH PRIVILEGES')
                    self.master_con.cursor.execute("FLUSH TABLES WITH READ LOCK")
                    self.master_con.cursor.execute("SHOW MASTER STATUS")
                    res = self.master_con.cursor.fetchall()
                    master_binlog = res[0][0]
                    master_log_pos = res[0][1]              
                    self.master_con.cursor.execute("UNLOCK TABLES")
                    self.master_con.close()
                else:
                    print(f"[i] Found cofigured replica user on {self.d_mysql[server]['name']}. Skipping replication setup")
                    return False

            elif self.d_mysql[server]['name'] == 'sql-slave':
                self.slave_con.connect()
                self.slave_con.cursor.execute('SHOW SLAVE STATUS')
                res = self.slave_con.cursor.fetchall()
                if res == []:
                    self.slave_con.cursor.execute('SET GLOBAL server_id=2')
                    if len(master_binlog) > 1 and master_log_pos > 1 :
                        self.slave_con.cursor.execute(f" \
                            CHANGE MASTER TO \
                            MASTER_HOST='{self.d_mysql['master']['host']}', \
                            MASTER_USER='replica_user', \
                            MASTER_PASSWORD='{password}', \
                            MASTER_PORT={self.d_mysql['master']['port']}, \
                            MASTER_LOG_FILE='{master_binlog}', \
                            MASTER_LOG_POS={master_log_pos}, \
                            MASTER_CONNECT_RETRY=10; \
                        ")
                        self.slave_con.cursor.execute("START SLAVE")
                        self.slave_con.cursor.execute("SET @@GLOBAL.read_only=1")
                        self.slave_con.close()

                    if self._verify_mysql_replication():
                        print("[+] Replication has been configured and working")
                        return True
                    else:
                        print("[-] Something went wrong while setting up replication")
                        exit(1)
                else:
                    print("[i] MySQL Slave already configured. Skipping")
                    return False

    def create_users(self):
        self.master_con.connect()
        print("[i] Creating ProxySQL user on MySQL")
        self.master_con.cursor.execute(f"CREATE USER '{self.config['proxysql']['monitor_user']}'@'%' IDENTIFIED BY '{self.config['proxysql']['monitor_passwd']}';")
        print("[i] Creating App user on MySQL")
        self.master_con.cursor.execute(f"CREATE USER '{self.config['app']['mysql_user']}'@'%' IDENTIFIED BY '{self.config['app']['mysql_passwd']}';")
        self.master_con.cursor.execute(f"GRANT ALL PRIVILEGES ON *.* TO '{self.config['app']['mysql_user']}'@'%';")
        self.master_con.close()

    def run_migration(self, sql_file):
        mysql = MySQLConn(self.config['proxysql']['ssh_host'], self.config['app']['mysql_user'], self.config['app']['mysql_passwd'], 3306, 'proxy')
        mysql.connect()
        print("[i] Executing MySQL migration")
        with open('migrations/'+ sql_file) as batch:
            for b in batch:
                mysql.cursor.execute(b)
        mysql.connection.commit()
        mysql.close()

    def _verify_mysql_replication(self) -> bool:
        self.slave_con.connect()
        self.slave_con.cursor.execute('SHOW SLAVE STATUS')
        status = self.slave_con.cursor.fetchall()
        self.slave_con.close()
        if status == []:
            return False
        elif status[0][10] == 'Yes':
            return True
        else:
            return False
