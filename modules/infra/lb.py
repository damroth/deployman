from modules.connectors.ssh_connector import SshConn
from configparser import ConfigParser

class Lb:

    def __init__(self, config: ConfigParser):

        self.config       = config
        self.ssh          = SshConn(self.config['load_balancer']['ssh_host'], self.config['load_balancer']['ssh_user'])
        self.img_lb       = 'nginx:1.21-alpine'
        self.lb_name      = 'nginx'
        self.lb_conf      = 'state/lb.conf'


    def init_lb_container(self):
        nginx = self.ssh.run_command('docker ps --filter "name="' + self.lb_name + ' --format "{{.Names}}"')
        if self.lb_name in nginx:
            print("[i] Nginx container already running")
        else:
            nginx = self.ssh.run_command('docker run --rm -d --name=' + self.lb_name + ' -p 80:80 '+ self.img_lb)
            print(f"[+] Load balancer deployed: {nginx}")


    def deploy_config(self):
        print("[i] Deploying loadbalancer config")
        self.ssh.scp_file(self.lb_conf)
        self.ssh.run_command(f'docker cp lb.conf {self.lb_name}:///etc/nginx/nginx.conf')

    def reload_service(self):
        print("[i] Reloading loadbalancer service")
        self.ssh.run_command(f'docker exec {self.lb_name} nginx -s reload')

    def add_app(self, app_ip, app_port):
        selector = 0
        app = f"\t\tserver {app_ip}:{app_port};\n"
        with open(self.lb_conf, 'r') as file:
            for number, line in enumerate(file):
                if line.__contains__('upstream app {'):
                    selector = number
                    file.seek(0)
                    data = file.readlines()

        with open(self.lb_conf, 'w') as file:
            data.insert(selector +1, app)
            file.writelines(data)


    def remove_app(self, app_ip, app_port):  
        selector = 0
        app = f"\t\tserver {app_ip}:{app_port};\n"
        with open(self.lb_conf) as file:
            for number, line in enumerate(file):
                if line.__contains__(app):
                    selector = number
                    file.seek(0)
                    data = file.readlines()

        if selector > 0:
            with open(self.lb_conf, 'w') as file:
                del data[selector]
                file.writelines(data)
