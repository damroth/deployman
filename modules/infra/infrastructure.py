from configparser import ConfigParser
from modules.infra.lb import Lb
from modules.infra.mysql import MySQL
from modules.infra.proxysql import ProxySQL
from modules.connectors.ssh_connector import SshConn
from modules.deploy.deployer import Deployer, DeployerState
from modules.helpers import http_test
import time


class Infra:

    def __init__(self, config: ConfigParser):
        self.config = config


    def init_lb(self):
        lb = Lb(self.config)
        lb.init_lb_container()


    def init_mysql_infra(self):
        mysql = MySQL(self.config)
        mysql.init_mysql_containers()
        if mysql.setup_mysql_replication():
            mysql.create_users()


    def init_proxy(self):
        proxy = ProxySQL(self.config)
        proxy.init_proxysql_container()
        proxy.configure_proxy()


    def deploy_apps(self):
        if float(self.config['app']['version']) > 1.0:
            print("[i] Initial deploys are allowed only for 1.0 version")
            exit(1)
        mysql = MySQL(self.config)
        mysql.run_migration('db_init.sql')

        lb = Lb(self.config)
        apps = filter(lambda x: "instance_" in x, self.config.sections())
        for instance in apps:
            sshclient = SshConn(self.config[instance]['ssh_host'], self.config[instance]['ssh_user'])
            deployer = Deployer(self.config, instance, sshclient)
            deployer.deploy(lb)


    def upgrade_apps(self):
        lb = Lb(self.config)
        apps = set(filter(lambda x: "instance_" in x, self.config.sections()))
        deployer_state = DeployerState()
        state = set(deployer_state.apps.keys())
        added = apps - state
        removed = state - apps
        if not (removed == set('') and added == set('')):
            print("[-] Can't upgrade - inconsistency detected between config and state. Run rescale first.")
            exit(1)

        version_verified = []
        for instance in deployer_state.apps:
            if (deployer_state.apps[instance]['version'] < self.config['app']['version']):
                version_verified.append(True)
            else:
                version_verified.append(False)
        if False in version_verified:
            print("[-] Runnig version is higher or equal than requested version")
            exit(1)

        length = len(deployer_state.apps.keys())
        if length < 2:
            print("[-] For zero downtime upgrade there should be minimum 2 app instances.")
            exit(1)

        mysql = MySQL(self.config)
        mysql.run_migration(f"migration_{self.config['app']['version']}_before.sql")

        # Split list of running containers in half
        index = length // 2
        app_list = list(state)
        first = app_list[:index]
        second = app_list[index:]

        # remove first half 
        for section in first:
            sshclient = SshConn(self.config[section]['ssh_host'], self.config[section]['ssh_user'])
            deployer = Deployer(self.config, section, sshclient)
            deployer.destroy(lb)

        # upgrade first half, but don't deploy config
        for section in first:
            sshclient = SshConn(self.config[section]['ssh_host'], self.config[section]['ssh_user'])
            deployer = Deployer(self.config, section, sshclient)
            deployer.deploy(lb, False)

            time.sleep(1) # a little bit of time for app to become available
            if not http_test(f"http://{deployer.ssh.host}:{deployer.app_port}", f"{deployer.app_version}"):
                print("[-] Can't deploy app. Stopping upgrade process.")
                exit(1)

 
        # removing second section from config for traffic swapping
        for section in second:
            deployer = Deployer(self.config, section, sshclient)
            deployer.verify() # needed for port setter
            lb.remove_app(deployer.ssh.host, deployer.app_port)


        # push new containers to lb without the old ones
        # at this point we should have exposed only the new version
        lb.deploy_config()
        lb.reload_service()


        # redeploy second half
        for section in second:
            sshclient = SshConn(self.config[section]['ssh_host'], self.config[section]['ssh_user'])
            deployer = Deployer(self.config, section, sshclient)
            deployer.destroy(lb, False)
            deployer.deploy(lb)

            time.sleep(1)
            for instance in apps:
                if not http_test(f"http://{deployer.ssh.host}:{deployer.app_port}", f"{deployer.app_version}"):
                    print("[-] Failed to upgrade")
                    exit(1)

        mysql.run_migration(f"migration_{self.config['app']['version']}_after.sql")

        
    def rescale_apps(self):
        lb = Lb(self.config)
        apps = set(filter(lambda x: "instance_" in x, self.config.sections()))
        deployer_state = DeployerState()
        state = set(deployer_state.apps.keys())
        if state == set(''):
            print("[-] Please deploy applications before rescale.")
            exit(1)
        added = apps - state
        removed = state - apps
        
        if added:
            for a in added:
                sshclient = SshConn(self.config[a]['ssh_host'], self.config[a]['ssh_user'])
                deployer = Deployer(self.config, a, sshclient)
                deployer.deploy(lb)
 
        if removed:
            for r in removed:
                sshclient = SshConn(deployer_state.apps[r]['host'], deployer_state.apps[r]['ssh_user'])
                deployer = Deployer(self.config, r, sshclient)
                deployer.destroy(lb)


    def destroy_apps(self):
        lb = Lb(self.config)
        apps = filter(lambda x: "instance_" in x, self.config.sections())
        for section in apps:
            sshclient = SshConn(self.config[section]['ssh_host'], self.config[section]['ssh_user'])
            deployer = Deployer(self.config, section, sshclient)
            deployer.destroy(lb)

