import secrets
import string
import requests
import time


def gen_passwd() -> str:
    alphabet = string.ascii_letters + string.digits
    return ''.join(secrets.choice(alphabet) for _ in range(20))


def http_test(url, ensure_txt) -> bool:
    r = requests.get(url)
    for _ in range(3):
        try:
            if (r.status_code == 200 and ensure_txt in r.text):
                return True
            else:
                return False
        except Exception as e:
            print(f"[i] Failed to get url. Restrying in 3 seconds")
            time.sleep(3)
        else:
            break
    else:
        print(f"[-] HTTP Test. Can't connect to target {url}")
        raise ConnectionError(f"[-] HTTP Test. Can't connect to target {url}")
