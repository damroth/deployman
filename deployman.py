from modules.infra.infrastructure import Infra
from configparser import ConfigParser
import click


@click.group()
def cli():
    pass


@cli.command(name='init-infra')
def init_infra():
    """Initialize whole infrastructure"""
    infra.init_lb()
    infra.init_mysql_infra()
    infra.init_proxy()


@cli.command(name='deploy')
def deploy():
    """Initial deploy of all apllication instances"""
    infra.deploy_apps()


@cli.command(name='destroy')
def deploy():
    """Destroy all instances of application"""
    infra.destroy_apps()


@cli.command(name='upgrade')
def deploy():
    """Upgrade applications version"""
    infra.upgrade_apps()


@cli.command(name='rescale')
def deploy():
    """Rescale amount of application instances"""
    infra.rescale_apps()


if __name__ == '__main__':
    config = ConfigParser()
    config.read('config.ini')
    infra = Infra(config)

    cli()

